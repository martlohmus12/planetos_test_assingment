import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { itemsFetchData } from '../../actions/dashboard';
import DashboardItem from '../../components/dashboard/dashboardItem';
import config from '../../config';

class Dashboard extends Component {

  componentDidMount() {
    this.props.fetchData(config.PANELS_URL);
  }

  componentWillReceiveProps(nextProps) {
    clearTimeout(this.timeout);

    if (!nextProps.panels) {
      this.startPoll();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  startPoll() {
    this.timeout = setTimeout(() => this.props.fetchData(config.PANELS_URL), config.PANELS_TIMER);
  }

  render() {
    const { panels = [], haveErrors } = this.props;

    if (haveErrors) {
      return <p>404 panels not found</p>;
    }

    const displayPanels = panels.map((item) => (
      <DashboardItem data={item}/>
    ));

    return (
      <div className="dashboard">
        {displayPanels}
      </div>
    );
  }
}

Dashboard.PropTypes = {
  fetchData: PropTypes.function,
  panels: PropTypes.array,
  haveErrors: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    panels: state.solarPanel.panels,
    hasErrors: state.solarPanel.hasErrors,
    isLoading: state.solarPanel.isLoading
  };
}


function mapDispatchToProps(dispatch) {
  return {
    fetchData: (url) => dispatch(itemsFetchData(url))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
