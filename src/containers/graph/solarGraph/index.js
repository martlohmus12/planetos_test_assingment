import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LineChart from '../../../components/graphs/lineChart';
import { fetchSolarActivityData } from '../../../actions/sidebar/solarActivity';
import config from '../../../config';

class SolarGraph extends Component {

  componentDidMount() {
    this.props.fetchSolarData('http://api.planetos.com/v1/datasets/noaa_hrrr_surface_hourly/point?lon=-122.0&lat=37.3&apikey=' + config.API_KEY + '&var=Visible_Diffuse_Downward_Solar_Flux_surface&count=24');
  }

  componentWillReceiveProps(nextProps) {
    clearTimeout(this.timeout);

    if (!nextProps.solarActivity) {
      clearTimeout(this.timeout);
      this.startPoll();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  startPoll() {
    this.timeout = setTimeout(() =>
      this.props.fetchSolarData('http://api.planetos.com/v1/datasets/noaa_hrrr_surface_hourly/point?lon=-122.0&lat=37.3&apikey=' + config.API_KEY + '&var=Visible_Diffuse_Downward_Solar_Flux_surface&count=24'), config.WEATHER_FORECAST_TIMER);
  }

  render() {
    const { solarActivity } = this.props;

    let solarChart = null;

    if (typeof solarActivity !== 'undefined') {
      solarChart = <LineChart chartData={solarActivity.entries} unit={"W m-2"} dataValue={"Visible_Diffuse_Downward_Solar_Flux_surface"}/>;
    } else {
      solarChart = <div>..loading</div>;
    }

    return (
        <div className="lineChart">
            <p className="graphHeadline">Visible Diffuse Downward Solar Flux (in watts per square metre)</p>
            { solarChart }
        </div>
    );
  }
}

SolarGraph.PropTypes = {
  fetchSolarData: PropTypes.function,
  solarActivity: PropTypes.array
};


function mapStateToProps(state) {
  return {
    solarActivity: state.solarActivity.solar
  };
}


function mapDispatchToProps(dispatch) {
  return {
    fetchSolarData: (url) => dispatch(fetchSolarActivityData(url)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SolarGraph);

