import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchCloudCoverageData } from '../../../actions/sidebar/cloudCoverage';
import LineChart from '../../../components/graphs/lineChart';
import config from '../../../config';

class CloudGraph extends Component {

  componentDidMount() {
     this.props.fetchCloudData('http://api.planetos.com/v1/datasets/fmi_hirlam_surface/point?lon=26&lat=62.95&apikey=' + config.API_KEY + '&var=Total_cloud_cover_surface_layer&count=24');
  }

  componentWillReceiveProps(nextProps) {
    clearTimeout(this.timeout);

    if (!nextProps.cloudCoverage) {
      clearTimeout(this.timeout);
      this.startPoll();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  startPoll() {
    // this.timeout = setTimeout(() =>
      // this.props.fetchCloudData('http://api.planetos.com/v1/datasets/fmi_hirlam_surface/point?lon=26&lat=62.95&apikey=' + config.API_KEY + '&var=Total_cloud_cover_surface_layer&count=24'), 300000);
  }

  render() {
    const { cloudCoverage } = this.props;

    let cloudChart = null;
    if (typeof cloudCoverage !== 'undefined') {
      cloudChart = <LineChart chartData={cloudCoverage.entries} unit={"W m-2"} dataValue={"Total_cloud_cover_surface_layer"}/>;
    } else {
      cloudChart = <div>..loading</div>;
    }

    return (
        <div className="lineChart">
            <p className="graphHeadline">Sky cloud coverage (%)</p>
            <br/>
            <br/>
            { cloudChart }
        </div>
    );
  }
}

CloudGraph.PropTypes = {
  fetchCloudData: PropTypes.function,
  cloudCoverage: PropTypes.array
};

function mapStateToProps(state) {
  return {
    cloudCoverage: state.cloudCoverage.cloud
  };
}


function mapDispatchToProps(dispatch) {
  return {
    fetchCloudData: (url) => dispatch(fetchCloudCoverageData(url)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CloudGraph);
