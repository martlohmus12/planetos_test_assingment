import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TotalOutput from '../../components/sidebar/totalOutput';
import CloudGraph from '../graph/cloudGraph';
import SolarGraph from '../graph/solarGraph';

class Sidebar extends Component {

  render() {
    const { output } = this.props;

    return (
      <div className="sidebar">
        <TotalOutput value={ output }/>
        <div className="lineChart">
          <SolarGraph/>
          <CloudGraph/>
        </div>
      </div>
    );
  }
}

Sidebar.PropTypes = {
  output: PropTypes.Number
};

function mapStateToProps(state) {
  return {
    output: state.solarPanel.totalOutput
  };
}

export default connect(mapStateToProps)(Sidebar);
