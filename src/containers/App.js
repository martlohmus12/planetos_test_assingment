import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from './dashboard';
import Sidebar from './sidebar';

class App extends Component {
  render() {
    return (
		<div className="main-app-container">
		<Sidebar/>
		<Dashboard/>
		</div>
    );
  }
}

export default connect()(App);
