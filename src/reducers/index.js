import { combineReducers } from 'redux';

import { cloudCoverage } from './sidebar/cloudCoverage';
import { solarActivity } from './sidebar/solarActivity';
import { solarPanel } 	 from './dashboard';

const rootReducer = combineReducers({
  cloudCoverage,
  solarActivity,
  solarPanel,
});

export default rootReducer;
