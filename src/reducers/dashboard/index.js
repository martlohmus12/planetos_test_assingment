/* eslint-disable */

import * as actions from '../../actions/dashboard';

export function solarPanel(state = [], action) {
  switch (action.type) {
    case actions.PANEL_HAVE_ERRORS:
      return Object.assign({}, state, {
          haveErrors: action.haveErrors
      })
    case actions.PANEL_IS_LOADING:
      return Object.assign({}, state, {
          isLoading: action.isLoading
      })
    case actions.RECEIVE_SOLAR_PANELS:
      return Object.assign({}, state, {
          panels: action.panels
      })
    case actions.RECEIVE_TOTAL_OUTPUT:
      return Object.assign({}, state, {
          totalOutput: action.output
      })
    default:
      return state;
  }
}



