import * as actions from '../../../actions/sidebar/cloudCoverage';

export function cloudCoverage(state = [], action = []) {
  switch (action.type) {
    case actions.RECEIVE_CLOUD_COVERAGE:
      return Object.assign({}, state, {
        cloud: action.items
      });
    default:
      return state;
  }
}
