import * as actions from '../../../actions/sidebar/solarActivity';

export function solarActivity(state = [], action) {
  switch (action.type) {
    case actions.RECEIVE_SOLAR_ACTIVITY:
      return Object.assign({}, state, {
        solar: action.items
      });
    default:
      return state;
  }
}
