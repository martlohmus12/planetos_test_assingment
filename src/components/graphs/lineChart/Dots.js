import React, { Component } from 'react'

class Dots extends Component {
  render(){
    var _self = this;

    var data = this.props.data.splice(1);
    data.pop();
    var circles = data.map(function(d,i) {
      return (<circle className="dot" r="7" cx={_self.props.x(d.date)} cy={_self.props.y(d.count)} fill="#7dc7f4"
      stroke="#3f5175" strokeWidth="5px" key={i}
      onMouseOver={_self.props.showToolTip} onMouseOut={_self.props.hideToolTip}
      data-key={d3.time.format("%d %H %M")(d.date)} data-value={ d.count }/>)
    });

    return (
      <g>
          {circles}
      </g>
    );
  }
}

export default Dots;
