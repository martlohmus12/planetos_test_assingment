/* eslint-disable */
import React, { Component } from 'react'
import Dots from './Dots';
import Axis from './Axis';
import Grid from './Grid';
import ToolTip from './ToolTip';
import ReactDOM from 'react-dom';
import $ from "jquery";
import * as d3 from "d3";

class LineChart extends Component {
    
    constructor(props) {
      super(props);
    
      this.state = {
        tooltip:{ display:false,data:{key:'',value:''}},
        width:this.props.width
      };

      this.showToolTip = this.showToolTip.bind(this);
      this.hideToolTip = this.hideToolTip.bind(this);
    }

    componentWillMount(){
      var _self=this;

      $(window).on('resize', function(e) {
          _self.updateSize();
      });

      this.setState({width:this.props.width});
    }

    componentDidMount() {
      this.updateSize(); 
    }
    componentWillUnmount(){
      $(window).off('resize');
    }

    updateSize(){
      var node = ReactDOM.findDOMNode(this);
      var parentWidth=$(node).width();

      if(parentWidth<this.props.width){
          this.setState({width:parentWidth});
      }else{
          this.setState({width:this.props.width});
      }
    }

    showToolTip(e){
      e.target.setAttribute('fill', '#FFFFFF');

      this.setState({tooltip:{
          display:true,
          data: {
              key:e.target.getAttribute('data-key'),
              value:e.target.getAttribute('data-value')
              },
          pos:{
              x:e.target.getAttribute('cx'),
              y:e.target.getAttribute('cy')
          }

          }
      });
    }

    hideToolTip(e){
      e.target.setAttribute('fill', '#7dc7f4');
      this.setState({tooltip:{ display:false,data:{key:'',value:''}}});
    }

    render(){
      const { chartData, dataValue, unit } = this.props;
      
      let newData = [];
      chartData.map(function(obj) { 
          newData.push({day: obj.axes.time, count: obj.data[dataValue] });
      });
      var data = newData;

      var margin = {top: 30, right: 50, bottom: 20, left: 50},
          w = this.state.width - (margin.left + margin.right),
          h = this.props.height - (margin.top + margin.bottom);

      var parseDate = d3.time.format("%Y-%m-%dT%H:%M:%S").parse;

      data.forEach(function (d) {
          d.date = parseDate(d.day);
      });

      var x = d3.time.scale()
          .domain(d3.extent(data, function (d) {
              return d.date;
          }))
          .rangeRound([0, w]);

      var y = d3.scale.linear()
          .domain([0,d3.max(data,function(d){
              return d.count;
          })])
          .range([h, 0]);

      var yAxis = d3.svg.axis()
          .scale(y)
          .orient('left')
          .ticks(10);

      var xAxis = d3.svg.axis()
          .scale(x)
          .orient('bottom')
          .tickValues(data.map(function(d,i){
              if(i>0)
                  return d.date;
          }).splice(1))
          .ticks(4);

      var yGrid = d3.svg.axis()
          .scale(y)
          .orient('left')
          .ticks(14)
          .tickSize(-w, 0, 0)
          .tickFormat("");

      var line = d3.svg.line()
          .x(function (d) {
              return x(d.date);
          })
          .y(function (d) {
              return y(d.count);
          }).interpolate('cardinal');




      var transform='translate(' + margin.left + ',' + margin.top + ')';

      return (
          <div>
              <svg id={this.props.chartId} width={this.state.width} height={this.props.height}>

                  <g transform={transform}>

                      <Grid h={h} grid={yGrid} gridType="y"/>

                      <Axis h={h} axis={yAxis} axisType="y" />
                      <Axis h={h} axis={xAxis} axisType="x"/>

                      <path className="line shadow" d={line(newData)} strokeLinecap="round"/>

                      <Dots data={data} x={x} y={y} showToolTip={this.showToolTip} hideToolTip={this.hideToolTip}/>

                      <ToolTip unit={ unit } tooltip={this.state.tooltip}/>
                  </g>

              </svg>


          </div>
      );
    }
};

LineChart.defaultProps = {
  width: 500,
  height: 200,
  chartId: 'v1_chart'
}

export default LineChart;