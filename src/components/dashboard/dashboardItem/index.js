/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const DashbboardItem = (props) => (
  <div className="dashbboardItem">
    <p>id: { props.data.id }</p>
    <p>voltage: { props.data.voltage } V</p>
    <p>Watt: { props.data.wattage } W</p>
  </div>
);

DashbboardItem.PropTypes = {
  value: PropTypes.object
};

export default DashbboardItem