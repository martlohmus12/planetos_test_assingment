import React from 'react';
import PropTypes from 'prop-types';

function wattsToKw(watts) {
  return watts / 1000;
}

const TotalOutput = ({ value }) => {
  const kw = wattsToKw(value);

  return (
    <div className="totalOutput">
      <p>Total output: { kw } kW</p>
    </div>
  );
};

TotalOutput.PropTypes = {
  value: PropTypes.Number
};


export default TotalOutput;
