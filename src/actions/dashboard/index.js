export const PANEL_HAS_ERRORS = 'PANEL_HAS_ERRORS';
export const PANEL_IS_LOADING = 'PANEL_IS_LOADING';
export const RECEIVE_SOLAR_PANELS = 'RECEIVE_SOLAR_PANELS';
export const RECEIVE_TOTAL_OUTPUT = 'RECEIVE_TOTAL_OUTPUT';

export function haveErrors(bool) {
  return {
    type: 'PANEL_HAVE_ERRORS',
    haveErrors: bool
  };
}

export function isLoading(bool) {
  return {
    type: 'PANEL_IS_LOADING',
    isLoading: bool
  };
}

export function receiveAllSolarPanels(panels) {
  return {
    type: 'RECEIVE_SOLAR_PANELS',
    panels
  };
}

export function receiveTotalOutput(output) {
  return {
    type: 'RECEIVE_TOTAL_OUTPUT',
    output
  };
}

export function getTotalOutput(panels) {
  return panels.map(function(a) {
    return a.voltage;
  })
  .reduce(function(a, b) {
    return a + b;
  });
}

export function itemsFetchData(url) {
  return (dispatch) => {
    fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then((response) => response.json())
    .then(function (panels) {
        dispatch(receiveAllSolarPanels(panels));
        dispatch(receiveTotalOutput(getTotalOutput(panels)));
    })
    .catch((error) => dispatch(haveErrors(error)));
  };
}
