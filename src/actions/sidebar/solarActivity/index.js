export const RECEIVE_SOLAR_ACTIVITY = 'RECEIVE_SOLAR_ACTIVITY';
export const ITEM_HAS_ERRORS = 'ITEM_HAS_ERRORS';

export function solarActivityHasErrors(bool) {
  return {
    type: 'ITEM_HAS_ERRORS',
    hasErrors: bool
  };
}

export function itemsFetchData(items) {
  return {
    type: 'RECEIVE_SOLAR_ACTIVITY',
    items
  };
}

export function fetchSolarActivityData(url) {
  return (dispatch) => {
    fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText);
      }

      return response;
    })
    .then((response) => response.json())
    .then((items) =>
      dispatch(itemsFetchData(items))
    )
    .catch(() => dispatch(SolarActivityHasErrors(bool)));
  };
}
