export const RECEIVE_CLOUD_COVERAGE = 'RECEIVE_CLOUD_COVERAGE';
export const ITEM_HAS_ERRORS = 'ITEM_HAS_ERRORS';

export function cloudCoverageHasErrors(bool) {
  return {
    type: 'ITEM_HAS_ERRORS',
    hasErrors: bool
  };
}

export function itemsFetchData(items) {
  return {
    type: 'RECEIVE_CLOUD_COVERAGE',
    items
  };
}

export function fetchCloudCoverageData(url) {
  return (dispatch) => {
    fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText);
      }

      return response;
    })
    .then((response) => response.json())
    .then((items) =>
      dispatch(itemsFetchData(items))
    )
    .catch(() => dispatch(cloudCoverageHasErrors(true) ));
  };
}

